#!/usr/bin/env python

'''
Simple "Square Detector" program.

Loads several images sequentially and tries to find squares in each image.
'''

# Python 2/3 compatibility
from __future__ import print_function
import sys
PY3 = sys.version_info[0] == 3

if PY3:
    xrange = range

import numpy as np
import cv2 as cv
import os
from flask import Flask, flash, request, redirect, render_template,jsonify 
from werkzeug.utils import secure_filename

app=Flask(__name__)

app.secret_key = "secret key"
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

# Get current path
path = os.getcwd()
# file Upload
UPLOAD_FOLDER = os.path.join(path, 'uploads')

# Make directory if uploads is not exists
if not os.path.isdir(UPLOAD_FOLDER):
    os.mkdir(UPLOAD_FOLDER)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# Allowed extension you can set your own
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

# MicroAlbumin values
C1_VALUES = [10,30,80,150]

# Creatinine values
C2_VALUES = [10, 50, 100, 200, 300]

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def upload_form():
    return render_template('upload.html')


@app.route('/', methods=['POST'])
def upload_file():
    if request.method == 'POST':

        if 'files[]' not in request.files:
            flash('No file part')
            return redirect(request.url)

        files = request.files.getlist('files[]')
        if len(files) != 2:
            return "Require two images", 400
        colors1 =[]
        colors2 =[]
        color1 = []
        color2 = []
        for file in files:
            if "colorchart" in file.filename:
                img = cv.imdecode(np.fromstring(file.read(), np.uint8), cv.IMREAD_UNCHANGED)
                cenList1, cenList2 = find_squares(img)
                colors1 = [img[centroid] for centroid in cenList1]
                colors2 = [img[centroid] for centroid in cenList2]
            if "teststrip" in file.filename:
                img = cv.imdecode(np.fromstring(file.read(), np.uint8), cv.IMREAD_UNCHANGED)
                height, width, color = img.shape
                color1 = img[(height//4, width//2)]
                color2 = img[(height//4 * 3, width//2)]
        if colors1 == [] or colors2 == [] or color1 == [] or color2 ==[]:
            return "bad image(s)", 400
            
        index1 = find_nearest(colors1, color1)
        index2 = find_nearest(colors2, color2)
        C1 = C1_VALUES[index1]
        C2 = C2_VALUES[index2]
        flash('Files processed')
        data = {"MicroAlbumin": C1, "Creatinine" : C2}
        return jsonify(data), 200
        # for file in files:
        #     if file and allowed_file(file.filename):
        #         filename = secure_filename(file.filename)
        #         file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

def find_nearest(colors, newColor):
    diff = 255*3
    indexFound = -1
    for index, color in enumerate(colors):
        newDiff = abs(color[0] - newColor[0]) +abs(color[1] - newColor[1]) + abs(color[2] - newColor[2])
        if newDiff < diff:
            indexFound = index
            diff = newDiff
    return indexFound

def angle_cos(p0, p1, p2):
    d1, d2 = (p0-p1).astype('float'), (p2-p1).astype('float')
    return abs( np.dot(d1, d2) / np.sqrt( np.dot(d1, d1)*np.dot(d2, d2) ) )

def find_squares(img):
    img = cv.GaussianBlur(img, (5, 5), 0)
    squares = []
    centroidsList1 = []
    centroidsList2 = []
    centroids = []
    for gray in cv.split(img):
        for thrs in xrange(0, 255, 26):
            if thrs == 0:
                bin = cv.Canny(gray, 0, 90, apertureSize=5)
                bin = cv.dilate(bin, None)
            else:
                _retval, bin = cv.threshold(gray, thrs, 255, cv.THRESH_BINARY)
            contours, _hierarchy = cv.findContours(bin, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
            for cnt in contours:
                cnt_len = cv.arcLength(cnt, True)
                cnt = cv.approxPolyDP(cnt, 0.02*cnt_len, True)
                #print(cv.contourArea(cnt))
                if len(cnt) == 4 and cv.contourArea(cnt) > 1000 and cv.contourArea(cnt) <20000 and cv.isContourConvex(cnt):
                    cnt = cnt.reshape(-1, 2)
                    max_cos = np.max([angle_cos( cnt[i], cnt[(i+1) % 4], cnt[(i+2) % 4] ) for i in xrange(4)])
                    if max_cos < 0.1:
                        square = np.int0(cv.boxPoints(cv.minAreaRect(cnt)))
                        centroid = (square[2][1]-(square[2][1]-square[0][1])//2, square[2][0]-(square[2][0]-square[0][0])//2)
                        if addNewCentroid(centroids, centroid):
                            centroids.append(centroid)
                            squares.append(square)
                            
                            #print(centroid)
                        #cv.drawContours( img, squares, -1, (0, 255, 0), 3 )
                        #cv.imshow('squares', img)
                        #cv.waitKey(1)
    centroids.sort()
    print(centroids)
    centroidsList1 = centroids[0:4]
    centroidsList2 = centroids[4:]

            
    return centroidsList1, centroidsList2


def addNewCentroid(centroids, newCen):
    if centroids == []:
        return True
    for centroid in centroids:
        if abs(centroid[0] - newCen[0]) < 10 and abs(centroid[1] - newCen[1]) < 10:
            return False
    return True

# def isRectangleOverlap(R1, R2):
#     if (R1[0]>=R2[2]) or (R1[2]<=R2[0]) or (R1[3]<=R2[1]) or (R1[1]>=R2[3]):
#         return False
#     else:
#         return True
    
# def includeRect(squares, newRect):
#     R1 = []
#     R2 = []
#     if squares == []:
#         return True
#     for square in squares:
#         if square[0][1] < square [1][1]:
#             R1 = [square[0][0], square[0][1],square[2][0], square[2][1]]
#         else:
#             R1 = [square[1][0], square[1][1],square[3][0], square[3][1]]
#         if newRect[0][1] < newRect [1][1]:
#             R2 = [newRect[0][0], newRect[0][1],newRect[2][0], newRect[2][1]]
#         else:
#             R2 = [newRect[1][0], newRect[1][1],newRect[3][0], newRect[3][1]]

#         if isRectangleOverlap(R1, R2):
#             return False
#     return True
            


def main():
    from waitress import serve
    serve(app, host='127.0.0.1',port = 5000)


if __name__ == '__main__':
    #app.run(host='127.0.0.1',port=5000,debug=False,threaded=True)
    #print(__doc__)
    main()
    #cv.destroyAllWindows()
